/*global AJS */
AJS.toInit(function() {
    var $ = AJS.$;

    var message = $('.aui-message.info.closeable:has(#other-users-span)');
    message.removeClass('info').addClass('error');
    message.find('.aui-icon.icon-warning').removeClass('icon-warning').addClass('icon-error');
});
